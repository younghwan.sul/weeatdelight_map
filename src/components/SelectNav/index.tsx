import React, { ReactElement } from "react";
import styled from "styled-components";

export interface Menu {
  label: string;
  selected: boolean;
  isShow: boolean;
}

interface Props {
  isShowAll?: boolean;
  title?: string;
  menus: {
    seletedCount: number;
    list: Menu[];
  };
  changeSeleted: (value: "all" | "removeAll" | number) => void;
  suffix?: ReactElement;
}
const SelectNav: React.FC<Props> = ({
  isShowAll = true,
  title,
  menus,
  changeSeleted,
  suffix,
}) => {
  const handleClick = (value: number | "all" | "removeAll") => {
    changeSeleted(value);
  };
  return (
    <SelectContainer>
      {title && <h2>{title}</h2>}
      <SelectList>
        {isShowAll && (
          <li
            className={
              menus.seletedCount === menus.list.length ? "selected" : ""
            }
            onClick={() =>
              handleClick(
                menus.seletedCount === menus.list.length ? "removeAll" : "all"
              )
            } // 리무브 올 여기서 결정
          >
            전체보기
          </li>
        )}
        {menus.list.map((el, idx) =>
          el.isShow ? (
            <li
              onClick={() => handleClick(idx)}
              key={idx}
              className={el.selected ? "selected" : ""}
            >
              {el.label}
            </li>
          ) : (
            <></>
          )
        )}
      </SelectList>
      {suffix}
    </SelectContainer>
  );
};

export default SelectNav;
const SelectContainer = styled.nav`
  width: 200px;
  height: 100%;
  overflow: scroll;
  text-align: center;
  h2 {
    margin: 12px 0;
  }
`;
const SelectList = styled.ul`
  padding: 0 8px;
  li {
    font-size: 12px;
    border-bottom: 1px solid #ddd;
    padding: 12px 0;
    cursor: pointer;
    &:first-of-type {
      border-top: 1px solid #ddd;
    }
    &:hover {
      background: #eee;
    }
    &.selected {
      background-color: #74b9ff;
      /* color: white; */
    }
  }
`;
