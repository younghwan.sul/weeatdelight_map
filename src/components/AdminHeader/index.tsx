import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { logoutUserDB } from "../../shared/redux/async/user";
import { RootState } from "../../shared/redux/configureStore";
import { resetAddress } from "../../shared/redux/Modules/addressSlice";
import DrawerNav from "../DrawerNav";

interface Props {}

const MapHeader: React.FC<Props> = () => {
  const [isShow, setIsShow] = useState(false);
  const handleClick = () => setIsShow(true);
  const navigation = useNavigate();
  const dispatch = useDispatch();
  const { token } = useSelector((state: RootState) => state.user.user);
  const handleNavClick = (name: "home" | "assign" | "signout") => {
    switch (name) {
      case "home":
        setIsShow(false);
        dispatch(resetAddress());
        navigation("/");
        break;

      case "assign":
        setIsShow(false);
        dispatch(resetAddress());
        navigation("/assign");
        break;

      case "signout":
        dispatch(logoutUserDB(token || ""));
        navigation("/");
        break;

      default:
        break;
    }
  };
  return (
    <Header>
      <NavButton onClick={handleClick}>
        <img src="/images/hamberger.svg" alt="햄버거 메뉴" />
      </NavButton>
      <DrawerNav
        isShow={isShow}
        onClose={() => {
          setIsShow(false);
        }}
      >
        <NavHeader>
          <Link to="/">
            <img src="/images/logo.png" alt="로고" />
            <span>푸맵 test</span>
          </Link>
        </NavHeader>
        <NavList>
          <Nav>
            <li>
              <button onClick={(e) => handleNavClick("home")}>
                기사님 별 배정현황
              </button>
            </li>
            <li>
              <button onClick={(e) => handleNavClick("assign")}>
                배정하기
              </button>
            </li>
            <li>
              <button onClick={(e) => handleNavClick("signout")}>
                로그아웃
              </button>
            </li>
          </Nav>
        </NavList>
      </DrawerNav>
    </Header>
  );
};

export default MapHeader;

const Header = styled.header`
  height: 40px;
  display: flex;
  align-items: center;
`;
const NavButton = styled.button`
  border: 0;
  background: none;
  height: 80%;
  margin-left: 12px;
  cursor: pointer;
  img {
    height: 100%;
  }
`;
const NavHeader = styled.header`
  padding: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  a {
    text-decoration: none;
    color: black;
    font-size: 24px;
    display: flex;
    padding-right: 20px;
    align-items: center;
  }
  img {
    margin-right: 20px;
    width: 40px;
  }
`;
const NavList = styled.nav`
  padding: 20px 40px;
`;
const Nav = styled.ul`
  list-style: none;
  button {
    padding: 20px;
    background: none;
    color: black;
    font-size: 1em;
    cursor: pointer;
    border: none;
  }
`;
