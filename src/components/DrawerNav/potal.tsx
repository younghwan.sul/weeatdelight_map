import { ReactNode, ReactPortal } from "react";
import { createPortal } from "react-dom";

const ModalEl = document.querySelector("#modal") as Element;

const PotalNav = ({ children }: { children: ReactNode }): ReactPortal => {
  return createPortal(<>{children}</>, ModalEl);
};

export default PotalNav;
