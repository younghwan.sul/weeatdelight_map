import React from "react";
import styled from "styled-components";
import PotalNav from "./potal";

interface Props {
  isShow: boolean;
  onClose: () => void;
}

const DrawerNav: React.FC<Props> = ({ isShow, children, onClose }) => {
  return (
    <PotalNav>
      <Container isShow={isShow ? "block" : "none"}>
        <Dim onClick={() => onClose()} />
        <Contents>{children}</Contents>
      </Container>
    </PotalNav>
  );
};

export default DrawerNav;

const Container = styled.section<{ isShow: "block" | "none" }>`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
  display: ${(p) => p.isShow};
`;
const Dim = styled.section`
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.3);
`;
const Contents = styled.aside`
  position: absolute;
  top: 0;
  left: 0;
  height: 100vh;
  min-width: 250px;
  background-color: white;
`;
