import React, { useEffect, useRef } from "react";
import styled from "styled-components";
import _ from "lodash";
import { Point } from "../../../constants/weeatPd";
import SelectedCardList from "../selectCard";

declare global {
  interface Window {
    kakao: any;
  }
}

export interface PropsPoint extends Point {
  pointIdx: number;
  regionIdx: number;
}
interface Props {
  loading: boolean;
  pointList: PropsPoint[];
  clickPoint?: (regionIdx: number, pointIdx: number) => void;
  isShowToolbar?: boolean;
  startRectangle?: ({ x, y }: { x: number; y: number }) => void;
  endRectangle?: ({ x, y }: { x: number; y: number }) => void;
  selectedpointList?: Point[];
  infoContents?: string;
}

const KakaoMap: React.FC<Props> = ({
  loading,
  pointList,
  clickPoint = (regionIdx, pointIdx) => {
    return;
  },
  isShowToolbar = false,
  startRectangle,
  endRectangle,
  selectedpointList = [],
  infoContents,
}) => {
  const options = useRef({
    x: "127.0532504713",
    y: "37.5481744730279",
    level: 5,
  });
  console.log(pointList);
  const markerRef = useRef<any[]>([]);
  const mapRef = useRef<HTMLDivElement>(null);
  const kakaoMapRef = useRef<any>(null);
  const managerRef = useRef<any>(null);
  const toolBarRef = useRef<any>(null);

  useEffect(() => {
    const kakaoOptions = {
      center: new window.kakao.maps.LatLng(
        options.current.y,
        options.current.x
      ),
      level: options.current.level,
    };
    kakaoMapRef.current = new window.kakao.maps.Map(
      mapRef.current,
      kakaoOptions
    );
  }, []);
  useEffect(() => {
    const drawingOptions = {
      map: kakaoMapRef.current, // drawing manager로 그리기 요소를 그릴 map 객체
      drawingMode: [
        // drawing manager로 제공할 그리기 요소 모드
        window.kakao.maps.drawing.OverlayType.RECTANGLE,
      ],
      guideTooltip: ["draw", "drag"], // 사용자에게 제공할 그리기 가이드 툴팁
      rectangleOptions: {
        draggable: true,
        removable: true,
        editable: true,
        strokeColor: "#39f", // 외곽선 색
        fillColor: "#39f", // 채우기 색
        fillOpacity: 0.5, // 채우기색 투명도
      },
    };
    managerRef.current = new window.kakao.maps.drawing.DrawingManager(
      drawingOptions
    );
    toolBarRef.current = new window.kakao.maps.Drawing.Toolbox({
      drawingManager: managerRef.current,
    });
    const toolbar: HTMLDivElement = toolBarRef.current.getElement();
    if (isShowToolbar) {
      kakaoMapRef.current.addControl(
        toolbar,
        window.kakao.maps.ControlPosition.TOPLEFT
      );
      managerRef.current.addListener("drawstart", function (mouseEvent: any) {
        var coords = new window.kakao.maps.Coords(
          mouseEvent.coords.La,
          mouseEvent.coords.Ma
        ).toLatLng();
        startRectangle && startRectangle({ x: coords.La, y: coords.Ma });
      });
      managerRef.current.addListener("drawend", function (mouseEvent: any) {
        var coords = new window.kakao.maps.Coords(
          mouseEvent.coords.La,
          mouseEvent.coords.Ma
        ).toLatLng();
        endRectangle && endRectangle({ x: coords.La, y: coords.Ma });
        mouseEvent.target.remove();
      });
    }
    return () => {
      if (isShowToolbar) toolbar.remove();
    };
  }, [startRectangle, endRectangle, isShowToolbar]);
  useEffect(() => {
    const points = loading ? [] : pointList;
    const changePosition = () => {
      const position = kakaoMapRef.current.getCenter();
      const level = kakaoMapRef.current.getLevel();
      options.current = {
        x: String(position.La),
        y: String(position.Ma),
        level,
      };
    };
    window.kakao.maps.event.addListener(
      kakaoMapRef.current,
      "bounds_changed",
      _.debounce(changePosition, 200)
    );
    for (let i = 0; i < points.length; i++) {
      let imageSrc = "/images/base.png";
      if (points[i].selected === "selected") imageSrc = "/images/selected.png";
      if (points[i].selected === "assigned") imageSrc = "/images/assigned.png";

      const imageSize = new window.kakao.maps.Size(30, 30),
        imageOption = { offset: new window.kakao.maps.Point(15, 30) };
      const markerImage = new window.kakao.maps.MarkerImage(
        imageSrc,
        imageSize,
        imageOption
      );

      const marker = new window.kakao.maps.Marker({
        map: kakaoMapRef.current,
        image: markerImage,
        clickable: true,
        position: new window.kakao.maps.LatLng(points[i].y, points[i].x),
      });
      if (infoContents) {
        const iwContent = `<div style="padding:5px;">상자 ${points[i].list.length}개</div>`;
        const infowindow = new window.kakao.maps.InfoWindow({
          content: iwContent,
        });

        window.kakao.maps.event.addListener(marker, "mouseover", function () {
          infowindow.open(kakaoMapRef.current, marker);
        });
        window.kakao.maps.event.addListener(marker, "mouseout", function () {
          infowindow.close();
        });
        window.kakao.maps.event.addListener(marker, "click", function () {
          infowindow.close();
          clickPoint(points[i].regionIdx, points[i].pointIdx);
        });
      }
      markerRef.current.push(marker);
    }
    return () => {
      for (let i = 0; i < markerRef.current.length; i++) {
        markerRef.current[i].setMap(null);
      }
      markerRef.current = [];
    };
  }, [loading, pointList, options, clickPoint]);

  return (
    <MapWrapper ref={mapRef}>
      <SelectedCardList pointList={selectedpointList} />
    </MapWrapper>
  );
};

export default KakaoMap;

const MapWrapper = styled.div`
  width: 100%;
  height: calc(100vh - 40px);
`;
