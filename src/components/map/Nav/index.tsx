import React from "react";
import styled from "styled-components";
import { RegionMenu } from "../../../shared/redux/Modules/addressSlice";
import { PdMenu } from "../../../shared/redux/Modules/pdSlice";
import SelectNav from "../../SelectNav";

interface Props {
  selectRegion: (value: number | "all" | "removeAll") => void;
  pdList: PdMenu[];
  regionList: RegionMenu[];
}

const MapNav: React.FC<Props> = ({ selectRegion, pdList, regionList }) => {
  const regionCount = regionList.reduce(
    (prev, curr) => (curr.selected ? prev + 1 : prev),
    0
  );
  return (
    <>
      {(pdList[pdList.length - 1]?.selected || false) && (
        <RegionNavContainer>
          <SelectNav
            title="지역선택"
            menus={{
              seletedCount: regionCount,
              list: regionList.map((val) => ({
                selected: val.selected,
                label: val.label,
                isShow: true,
              })),
            }}
            changeSeleted={selectRegion}
          />
        </RegionNavContainer>
      )}
    </>
  );
};
export default MapNav;

const RegionNavContainer = styled.div`
  position: absolute;
  right: 200px;
  top: 0;
`;
