import React from "react";
import styled from "styled-components";
import { Point } from "../../../constants/weeatPd";
interface Props {
  pointList: Point[];
}
const SelectedCardList: React.FC<Props> = ({ pointList }) => {
  // const { addressList } = useSelector((state: RootState) => state.address);/
  // const point = addressList
  //   .filter((el) => el.selected)
  //   .reduce(
  //     (prev, curr) => [
  //       ...prev,
  //       ...curr.point.filter((pointEl) => pointEl.selected === "selected"),
  //     ],
  //     [] as Point[]
  //   );
  return (
    <CardContainer>
      <CardList>
        {pointList.map((el, idx) => {
          const reciever = el.list
            .map((list) => `[${list.customer_name}]`)
            .join(", ");
          return (
            <li key={idx}>
              <Card>
                <h4>
                  [{idx + 1}/{pointList.length}] {el.address_base}
                </h4>
                <Info>
                  배송순서: {el.selected !== "fixed" ? "순서없음" : idx}
                </Info>
                <Info>상자 개수 {el.list.length}개</Info>
                <InfoContainer>
                  <Info>수신자: {reciever}</Info>
                  {reciever.length > 43 && (
                    <section className="comment">{reciever}</section>
                  )}
                </InfoContainer>
              </Card>
            </li>
          );
        })}
      </CardList>
    </CardContainer>
  );
};

export default SelectedCardList;

const CardContainer = styled.section`
  position: absolute;
  z-index: 10;
  top: 20px;
  left: 20px;
  padding-bottom: 40px;
  max-height: calc(100% - 120px);
  overflow: scroll;
  -ms-overflow-style: none;
  scrollbar-width: none;
  &::-webkit-scrollbar {
    display: none;
  }
`;

const CardList = styled.ul`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  list-style: none;
`;
const Card = styled.section`
  width: 250px;
  height: 180px;
  background: white;
  border-radius: 8px;
  margin-bottom: 20px;
  visibility: visible;
  box-shadow: rgb(0 0 0 / 25%) 0px 1px 4px;
  box-sizing: border-box;
  padding: 20px;
`;
const Info = styled.p`
  margin-top: 10px;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  word-break: break-all;
`;
const InfoContainer = styled.section`
  width: 100%;
  position: relative;
  .comment {
    display: none;
    width: 100%;
    background: #eee;
    word-break: break-all;
    position: absolute;
    top: calc(100% + 8px);
    padding: 10px;
    &::after {
      bottom: 100%;
      left: 50%;
      border: solid transparent;
      content: " ";
      height: 0px;
      width: 0px;
      position: absolute;
      pointer-events: none;
      border-color: rgba(255, 74, 74, 0);
      border-bottom-color: #eee;
      border-width: 8px;
      transform: translateX(-50%);
    }
  }
  &:hover .comment {
    display: block;
  }
`;
