import React, { useRef, useState } from "react";
import styled from "styled-components";
import { PdMenu } from "../../shared/redux/Modules/pdSlice";

interface Props {
  pdList: PdMenu[];
  assignPd: (id: string) => void;
}

const AssignModal: React.FC<Props> = ({ pdList, assignPd }) => {
  const [selected, setSelected] = useState<null | string>(null);
  const isOpen = useRef(false);
  const dropBtnRef = useRef<HTMLElement>(null);
  const handleDropBtnClick = () => {
    if (isOpen.current) {
      isOpen.current = false;
      dropBtnRef.current?.blur();
      return;
    }
    isOpen.current = true;
  };
  const handleListClick = (id: string) => {
    setSelected(id);
  };
  const submitAssign = () => {
    if (selected === null) {
      alert("선택된 pd님이 없습니다.");
      return;
    }
    assignPd(selected);
  };
  return (
    <Modal>
      <h2>배정하기</h2>
      <section>
        <DropButton
          ref={dropBtnRef}
          tabIndex={0}
          onBlur={() => (isOpen.current = false)}
          onClick={handleDropBtnClick}
        >
          <p>
            {selected === null
              ? "선택안됨"
              : pdList.find((el) => el.id === selected)?.name}
          </p>
          <img src="/images/caret-down.svg" alt="열기" />
          <DropDown>
            {pdList
              .filter((el) => el.id !== "none")
              .map((el, idx) => (
                <li onClick={() => handleListClick(el.id)} key={idx}>
                  {el.name} {el.area}
                </li>
              ))}
          </DropDown>
        </DropButton>
        <SubmitBtn onClick={submitAssign}>배정</SubmitBtn>
      </section>
    </Modal>
  );
};

export default AssignModal;
const Modal = styled.section`
  padding: 30px;
  text-align: center;
`;

const DropButton = styled.section`
  margin-top: 20px;
  width: 180px;
  position: relative;
  border-radius: 5px;
  border: 1px solid rgba(0, 0, 0, 0.1);
  cursor: pointer;
  p {
    padding: 8px 0;
    margin: 0 auto;
  }
  img {
    position: absolute;
    top: 50%;
    right: 12px;
    transform: translateY(-50%);
    width: 12px;
  }
  ul {
    display: none;
  }
  &:focus-within ul {
    display: block;
  }
`;
const DropDown = styled.ul`
  width: 100%;
  position: absolute;
  top: calc(100% + 5px);
  background-color: white;
  border-radius: 5px;
  padding: 3px;
  list-style: none;
  /* border: 1px solid #dcdcdc; */
  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
  li {
    width: 100%;
    padding: 8px;
    cursor: pointer;
    &:hover {
      background: rgba(129, 236, 236, 0.7);
    }
  }
`;
const SubmitBtn = styled.button`
  border: 0;
  background: rgb(116, 185, 255);
  color: white;
  width: 100%;
  margin-top: 20px;
  height: 40px;
  border-radius: 8px;
  cursor: pointer;
  &:hover {
    background: rgb(9, 132, 227);
  }
`;
