import React from "react";
import styled from "styled-components";
interface Props {
  isShow: boolean;
  closeModal: () => void;
}
const ModalContainer: React.FC<Props> = ({ children, closeModal, isShow }) => {
  if (!isShow) return <></>;
  return (
    <Container>
      <Dim onClick={closeModal} />
      <Modal>{children}</Modal>
    </Container>
  );
};

export default ModalContainer;

const Container = styled.section`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
`;
const Dim = styled.section`
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, 0.3);
`;
const Modal = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  min-height: 200px;
  min-width: 200px;
  background-color: white;
`;
