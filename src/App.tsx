import React from "react";
import { useSelector } from "react-redux";
import Admin from "./pages/Admin";
import Pds from "./pages/pds";
import Public from "./pages/Public";
import { RootState } from "./shared/redux/configureStore";

const App: React.FC = () => {
  const {
    isFetching,
    user: { token },
  } = useSelector((state: RootState) => {
    return state.user;
  });
  if (isFetching) {
    return <>로딩화면</>;
  }
  if (token === "admin1234") {
    return <Admin />;
  }
  if (token === "weeat1234") {
    return <Pds />;
  }
  return <Public />;
};

export default App;
