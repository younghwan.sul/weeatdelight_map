// import { Point } from "../components/map/kakao/kakaoMap";
import { Direction } from "./address";

export interface Point {
  address_base: string;
  x: string;
  y: string;
  pd: {
    id: string;
    name: string;
  } | null;
  list: Direction[];
  selected: "assigned" | "free" | "selected" | "fixed";
}
export interface Pd {
  name: string;
  area: string;
  id: string;
  deliveryList: Point[];
}
interface Output {
  status: number;
  message: string;
  data?: any;
}

export const pds: Pd[] = [
  {
    name: "김티버",
    area: "강남_01",
    id: "weeat01",
    deliveryList: [
      {
        address_base: "서울특별시 성동구 상원길 19 (성수동1가) 신한IT타워",
        x: "127.048772838829",
        y: "37.5458788603366",
        list: [
          {
            customer_name: "서유화",
            customer_phone: "01011111111",
            address_base: "서울특별시 성동구 상원길 19 (성수동1가) 신한IT타워",
            address_more: "8층 울트라브이",
            delivery_pos: "8층 울트라브이 사무실 앞",
            id: "wehudding0006",
            x: "127.048772838829",
            y: "37.5458788603366",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base: "서울특별시 성동구 아차산로 17 (성수동1가)",
        x: "127.046440454742",
        y: "37.5479607476645",
        list: [
          {
            customer_name: "윤규리,이상은,신우철,신동민,이상윤,최유선,이민지",
            customer_phone: "01011111111",
            address_base: "서울특별시 성동구 아차산로 17 (성수동1가)",
            address_more: " 서울숲 L-Towe 15층",
            delivery_pos: "15층 화장실 방향으로 복도 끝쪽 사무실",
            id: "wehudding0013",
            x: "127.046440454742",
            y: "37.5479607476645",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base: "서울특별시 성동구 상원6길 7 (성수동1가)",
        x: "127.048985430939",
        y: "37.5487675546973",
        list: [
          {
            customer_name: "김상완",
            customer_phone: "01011111111",
            address_base: "서울특별시 성동구 상원6길 7 (성수동1가)",
            address_more: "201호",
            delivery_pos: "공동현관없습니다",
            id: "wehudding0029",
            x: "127.048985430939",
            y: "37.5487675546973",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base: "서울특별시 성동구 성수일로 77(성수동1가) 서울숲IT밸리",
        x: "127.050343491959",
        y: "37.5474049437892",
        list: [
          {
            customer_name: "한인서",
            customer_phone: "01011111111",
            address_base:
              "서울특별시 성동구 성수일로 77(성수동1가) 서울숲IT밸리",
            address_more: "902호",
            delivery_pos: "문 앞",
            id: "wehudding0036",
            x: "127.050343491959",
            y: "37.5474049437892",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base:
          "서울특별시 성동구 서울숲2길 32-14(성수동1가, 갤러리아 포레)",
        x: "127.042914714267",
        y: "37.5456773286722",
        list: [
          {
            customer_name: "HONG JASON DONGJOON",
            customer_phone: "01011111111",
            address_base:
              "서울특별시 성동구 서울숲2길 32-14(성수동1가, 갤러리아 포레)",
            address_more: "A동 1층 103호",
            delivery_pos: "버거점프 매장안 / 대응배송 : 매장문앞",
            id: "wehudding0041",
            x: "127.042914714267",
            y: "37.5456773286722",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base: "서울특별시 성동구 서울숲길 54 (성수동1가) 성수그린빌",
        x: "127.043580039284",
        y: "37.5479284109517",
        list: [
          {
            customer_name: "김은진",
            customer_phone: "01011111111",
            address_base:
              "서울특별시 성동구 서울숲길 54 (성수동1가) 성수그린빌",
            address_more: "707호",
            delivery_pos: "문 앞(출입 비밀번호 없음)",
            id: "wehudding0044",
            x: "127.043580039284",
            y: "37.5479284109517",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base: "서울특별시 성동구 상원길 47(성수동1가, 중앙하이츠빌)",
        x: "127.04726623735",
        y: "37.547843740667",
        list: [
          {
            customer_name: "박은실",
            customer_phone: "01011111111",
            address_base:
              "서울특별시 성동구 상원길 47(성수동1가, 중앙하이츠빌)",
            address_more: "102동2203호",
            delivery_pos: "문앞",
            id: "wehudding0048",
            x: "127.04726623735",
            y: "37.547843740667",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base: "서울특별시 성동구 광나루로 166 (성수동1가)",
        x: "127.048557769654",
        y: "37.5513573808967",
        list: [
          {
            customer_name: "신혜원",
            customer_phone: "01011111111",
            address_base: "서울특별시 성동구 광나루로 166 (성수동1가)",
            address_more: "2층 블랭",
            delivery_pos: "2층 블랭",
            id: "wehudding0061",
            x: "127.048557769654",
            y: "37.5513573808967",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base: "서울특별시 성동구 왕십리로14길 22(성수동1가)",
        x: "127.045890587589",
        y: "37.5484935429606",
        list: [
          {
            customer_name: "이형준",
            customer_phone: "01011111111",
            address_base: "서울특별시 성동구 왕십리로14길 22(성수동1가)",
            address_more: "502호",
            delivery_pos: "공동현관문 #4569#",
            id: "wehudding0085",
            x: "127.045890587589",
            y: "37.5484935429606",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base: "서울특별시 성동구 상원1길 26 (성수동1가) 서울숲 A타워",
        x: "127.048432321547",
        y: "37.546194350143",
        list: [
          {
            customer_name: "김승아,김수현,신지선,전양민,김은정",
            customer_phone: "01011111111",
            address_base:
              "서울특별시 성동구 상원1길 26 (성수동1가) 서울숲 A타워",
            address_more: "405호",
            delivery_pos: "405호 앞",
            id: "wehudding0088",
            x: "127.048432321547",
            y: "37.546194350143",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base: "서울특별시 성동구 왕십리로16길 9 (성수동1가)",
        x: "127.045193170666",
        y: "37.5503771690932",
        list: [
          {
            customer_name: "안서영",
            customer_phone: "01011111111",
            address_base: "서울특별시 성동구 왕십리로16길 9 (성수동1가)",
            address_more: "7층",
            delivery_pos:
              "현관비번: 1004*불들어오는숫자순서대로*/ 엘리베이터 옆 흰색 캐비넷 위,",
            id: "wehudding0094",
            x: "127.045193170666",
            y: "37.5503771690932",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base: "서울특별시 성동구 왕십리로 125(성수동1가) kd타워",
        x: "127.043893352541",
        y: "37.5488383035453",
        list: [
          {
            customer_name: "이보아",
            customer_phone: "01011111111",
            address_base: "서울특별시 성동구 왕십리로 125(성수동1가) kd타워",
            address_more: "",
            delivery_pos: "2층 페스트파이브 우편보관함",
            id: "wehudding0118",
            x: "127.043893352541",
            y: "37.5488383035453",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base: "서울특별시 성동구 왕십리로5길 3(성수동1가) 공공복합청사",
        x: "127.044349655211",
        y: "37.5464025531618",
        list: [
          {
            customer_name: "권세희",
            customer_phone: "01011111111",
            address_base:
              "서울특별시 성동구 왕십리로5길 3(성수동1가) 공공복합청사",
            address_more: "2층 주민센터",
            delivery_pos: "1층 무인발급기 옆 배너 뒤에 숨겨주세요",
            id: "wehudding0134",
            x: "127.044349655211",
            y: "37.5464025531618",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
      {
        address_base:
          "서울특별시 성동구 아차산로 38(성수동1가) 스파크플러스 성수 2호점",
        x: "127.048397833508",
        y: "37.5465839562284",
        list: [
          {
            customer_name: "이재훈,김진우",
            customer_phone: "01011111111",
            address_base:
              "서울특별시 성동구 아차산로 38(성수동1가) 스파크플러스 성수 2호점",
            address_more: "3층 302호",
            delivery_pos: "3층 입구 옆 택배함 위",
            id: "wehudding0138",
            x: "127.048397833508",
            y: "37.5465839562284",
            region: "성동구",
            pd: null,
            fixed: false,
          },
        ],
        pd: null,
        selected: "selected",
      },
    ],
  },
  {
    name: "김토니",
    area: "성동_01",
    id: "weeat02",
    deliveryList: [],
  },
  {
    name: "김토니",
    area: "성동_02",
    id: "weeat03",
    deliveryList: [],
  },
  {
    name: "김토니",
    area: "성동_03",
    id: "weeat04",
    deliveryList: [],
  },
  {
    name: "김토니",
    area: "성동_07",
    id: "weeat05",
    deliveryList: [],
  },
  {
    name: "김토니",
    area: "성동_04",
    id: "weeat06",
    deliveryList: [],
  },
  {
    name: "김토니",
    area: "성동_05",
    id: "weeat07",
    deliveryList: [],
  },
  {
    name: "김토니",
    area: "성동_06",
    id: "weeat08",
    deliveryList: [],
  },
];

const setPds = () => {
  const pdList = [...pds];
  const setPoint = async (
    id: string,
    points: Point | Point[]
  ): Promise<Output> => {
    if (!id || !points) {
      return {
        status: 400,
        message: "wrong parameter",
      };
    }
    pdList.forEach((el) => {
      if (el.id === id) {
        if (Array.isArray(points)) {
          el.deliveryList = [...el.deliveryList, ...points];
        } else {
          el.deliveryList.push(points);
        }
      }
    });
    return {
      status: 200,
      message: "OK",
      data: JSON.parse(JSON.stringify(pdList)),
    };
  };
  const removePoint = async (
    id: string,
    points: Point | Point[]
  ): Promise<Output> => {
    if (!id || !points) {
      return {
        status: 400,
        message: "wrong parameter",
      };
    }
    pdList.forEach((el) => {
      if (el.id === id) {
        if (Array.isArray(points)) {
          el.deliveryList = el.deliveryList.filter((delivery) => {
            return (
              points.findIndex(
                (pointEl) => pointEl.address_base === delivery.address_base
              ) === -1
            );
          });
        } else {
          el.deliveryList = el.deliveryList.filter(
            (delivery) => delivery.address_base !== points.address_base
          );
        }
      }
    });
    return {
      status: 200,
      message: "OK",
    };
  };
  const getPd = async (id?: string): Promise<Output> => {
    if (!id) {
      return {
        status: 200,
        message: "OK",
        data: JSON.parse(JSON.stringify(pdList)),
      };
    }
    const pd = pdList.find((el) => el.id === id);
    if (pd === undefined) {
      return {
        status: 400,
        message: "undefined pd id",
      };
    }
    return {
      status: 200,
      message: "OK",
      data: JSON.parse(JSON.stringify(pd)),
    };
  };

  const getMyDelivery = async (id: string): Promise<Output> => {
    console.log(pdList);
    const pd = pdList.find((el) => el.id === id);
    console.log(pd);
    if (pd === undefined) {
      return {
        status: 400,
        message: "undefined pd id",
      };
    }
    return {
      status: 200,
      message: "OK",
      data: JSON.parse(
        JSON.stringify(
          pd.deliveryList.map((el) => ({
            ...el,
            pd: { name: pd.name, id: pd.id },
          }))
        )
      ),
    };
  };

  const fixMyDelivery = async (
    id: string,
    indexList: number[]
  ): Promise<Output> => {
    const pdIdx = pdList.findIndex((el) => el.id === id);
    if (pdIdx === -1) {
      return {
        status: 400,
        message: "undefined pd id",
      };
    }

    pdList[pdIdx].deliveryList = indexList.reduce(
      (prev, curr) => [...prev, pdList[pdIdx].deliveryList[curr]],
      [] as Point[]
    );
    return {
      status: 200,
      message: "OK",
      data: JSON.parse(JSON.stringify(pdList[pdIdx].deliveryList)),
    };
  };
  return { setPoint, removePoint, getPd, getMyDelivery, fixMyDelivery };
};

const pdsApi = {
  ...setPds(),
};
export default pdsApi;
