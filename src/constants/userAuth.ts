export declare namespace LoginProps {
  interface Input {
    id: string;
    password: string;
  }
  interface Output {
    status: number;
    message: string;
    data?: string;
  }
}

const user = () => {
  const users = {
    admin: {
      password: "1234",
      token: "admin1234",
    },
    weeat: {
      password: "1234",
      token: "weeat1234",
    },
  };
  const login = async ({
    id,
    password,
  }: LoginProps.Input): Promise<LoginProps.Output> => {
    if (!Object.keys(users).includes(id)) {
      return {
        status: 400,
        message: "wrong id",
      };
    }
    const userId = id as "admin" | "weeat";
    if (users[userId].password !== password) {
      return {
        status: 400,
        message: "wrong password",
      };
    }
    return {
      status: 200,
      message: "OK",
      data: users[userId].token,
    };
  };
  const logout = async (token: string): Promise<LoginProps.Output> => {
    return {
      status: 200,
      message: "OK",
    };
  };
  return {
    login,
    logout,
  };
};

const auth = {
  userLogin: user(),
};
export default auth;
