import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { store } from "./shared/redux/configureStore";
import GlobalThemeProvider from "./shared/style/Global";
import userSlice from "./shared/redux/Modules/userSlice";

import { BrowserRouter } from "react-router-dom";
const localToken = localStorage.getItem("userTokenTmp");
store.dispatch(userSlice.actions.setUser({ token: localToken || undefined }));
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <GlobalThemeProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </GlobalThemeProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
