import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import KakaoMap from "../../components/map/kakao/kakaoMap";
import PdHeader from "../../components/PdHeader";
import { getMyList } from "../../shared/redux/async/pd";
import { RootState } from "../../shared/redux/configureStore";

const DeliveryPage: React.FC = () => {
  const { isFetching, list } = useSelector(
    (state: RootState) => state.user.myList
  );
  const [fixed, setFixed] = useState<undefined | number[]>(
    new Array(list?.length || 0)
  );
  console.log(fixed);
  const dispatch = useDispatch();
  useEffect(() => {
    if (isFetching) dispatch(getMyList({ id: "weeat01" }));
  }, [isFetching, dispatch]);
  return (
    <>
      <PdHeader />
      <MapContainer>
        <KakaoMap
          selectedpointList={list || []}
          loading={isFetching}
          pointList={
            list?.map((val, pointIdx) => ({
              ...val,
              regionIdx: 0,
              pointIdx,
            })) || []
          }
        />

        {/* <AssignBtn onClick={handleAssignBtn}>배정하기</AssignBtn> */}
      </MapContainer>
    </>
  );
};

export default DeliveryPage;

const MapContainer = styled.section`
  position: relative;
  height: calc(100vh - 40px);
`;

const AssignBtn = styled.button`
  position: absolute;
  padding: 12px 18px;
  border-radius: 5px;
  border: 0;
  background-color: #f1c40f;
  font-size: 30px;
  color: white;
  bottom: 20px;
  left: 20px;
  z-index: 10;
  cursor: pointer;
`;
