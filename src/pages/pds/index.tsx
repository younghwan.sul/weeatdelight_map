import React from "react";
import { Routes, Route } from "react-router-dom";
import DeliveryPage from "./DeliveryList";

const Pds: React.FC = () => {
  return (
    <Routes>
      <Route path="/">
        <Route index element={<DeliveryPage />} />
      </Route>
    </Routes>
  );
};

export default Pds;
