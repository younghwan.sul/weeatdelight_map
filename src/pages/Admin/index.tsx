import React from "react";

import { Routes, Route } from "react-router-dom";
import AdminMap from "./AdminMap";
import AssignMap from "./AssignMap";

const Admin: React.FC = () => {
  return (
    <Routes>
      <Route path="/">
        <Route index element={<AdminMap />} />
        <Route path="assign" element={<AssignMap />} />
      </Route>
    </Routes>
  );
};

export default Admin;
