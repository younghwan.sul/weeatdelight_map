import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import MapHeader from "../../components/AdminHeader";
import KakaoMap, { PropsPoint } from "../../components/map/kakao/kakaoMap";
import SelectNav from "../../components/SelectNav";
import { getPdData } from "../../shared/redux/async/pd";
import { RootState } from "../../shared/redux/configureStore";
import { selectPd } from "../../shared/redux/Modules/pdSlice";

const AdminMap: React.FC = () => {
  const dispatch = useDispatch();
  const { isFetching: loading, pdList } = useSelector(
    (state: RootState) => state.pd
  );

  useEffect(() => {
    const getData = async () => {
      if (loading) dispatch(getPdData());
    };
    getData();
  }, [dispatch, loading]);
  const selectList = (value: number | "all" | "removeAll") => {
    if (value === "all" || value === "removeAll") return;
    dispatch(selectPd({ id: pdList[value].id }));
  };

  if (loading) {
    return <>로딩중</>;
  }

  const pdSelectedIdx = pdList.findIndex((val) => val.selected);
  const points: PropsPoint[] =
    pdList
      .find((val) => val.selected)
      ?.deliveryList.map((val, pointIdx) => ({
        ...val,
        regionIdx: pdSelectedIdx,
        pointIdx,
      })) || [];
  return (
    <>
      <MapHeader />
      <MapContainer>
        <KakaoMap
          selectedpointList={points}
          loading={loading}
          pointList={points}
        />
        <NavContainer>
          <SelectNav
            isShowAll={false}
            title="PD님 선택"
            menus={
              loading
                ? {
                    seletedCount: 0,
                    list: [],
                  }
                : {
                    seletedCount: 0,
                    list: pdList.map((el) => ({
                      label: el.name,
                      selected: el.selected,
                      isShow: true,
                    })),
                  }
            }
            changeSeleted={selectList}
          />
        </NavContainer>
      </MapContainer>
    </>
  );
};

export default AdminMap;

const MapContainer = styled.section`
  position: relative;
  height: calc(100vh - 40px);
  padding-right: 200px;
`;

const NavContainer = styled.div`
  position: absolute;
  right: 0;
  top: 0;
`;
