import React, { useCallback, useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import Swal from "sweetalert2";
import MapHeader from "../../components/AdminHeader";
import KakaoMap, { PropsPoint } from "../../components/map/kakao/kakaoMap";
import AssignModal from "../../components/modal/assignModal";
import SelectNav from "../../components/SelectNav";
import { Point } from "../../constants/weeatPd";
import useModal from "../../shared/hooks/useModal";
import { getAddressData, setAddressPd } from "../../shared/redux/async/adress";
import { getPdData, setAddressToPd } from "../../shared/redux/async/pd";
import { RootState } from "../../shared/redux/configureStore";
import {
  selectAddress,
  selectPoint,
  selectRectanglePoint,
} from "../../shared/redux/Modules/addressSlice";

const AssignMap: React.FC = () => {
  const { openModal, closeModal, ModalComponent: Modal } = useModal();
  const { isFetching: isPdFetching, pdList } = useSelector(
    (state: RootState) => state.pd
  );
  const { isFetching: isAddressFetching, addressList } = useSelector(
    (state: RootState) => state.address
  );
  const loading = isPdFetching || isAddressFetching;
  const coordsRef = useRef<{ x: number; y: number } | null>(null);
  const dispatch = useDispatch();
  const regionSelected: PropsPoint[] = addressList.reduce(
    (prev, curr, idx) =>
      curr.selected
        ? [
            ...prev,
            ...curr.point
              .map((val, pointIdx) => ({ ...val, regionIdx: idx, pointIdx }))
              .filter((val) => val.selected !== "assigned"),
          ]
        : prev,
    [] as PropsPoint[]
  );

  const regionCount = addressList.reduce(
    (prev, curr) => (curr.selected ? prev + 1 : prev),
    0
  );

  useEffect(() => {
    const getData = async () => {
      if (isPdFetching) dispatch(getPdData());
      if (isAddressFetching) dispatch(getAddressData());
    };
    getData();
  }, [dispatch, isPdFetching, isAddressFetching]);

  const selectRegion = (value: number | "all" | "removeAll") => {
    dispatch(selectAddress({ value }));
  };

  const clickPoint = (regionIdx: number, pointIdx: number) => {
    dispatch(selectPoint({ regionIdx, pointIdx }));
  };
  const startRectangle = useCallback(({ x, y }: { x: number; y: number }) => {
    coordsRef.current = { x, y };
  }, []);
  const endRectangle = useCallback(
    (end: { x: number; y: number }) => {
      coordsRef.current &&
        dispatch(
          selectRectanglePoint({
            start: { x: coordsRef.current.x, y: coordsRef.current.y },
            end,
          })
        );
      coordsRef.current = null;
    },
    [dispatch]
  );
  if (loading) return <>test</>;

  const handleAssignBtn = () => {
    openModal();
  };
  const submitAssign = async (pdId: string) => {
    const pd = pdList.find((el) => el.id === pdId);
    if (!pd) {
      Swal.fire({ title: "잘못된 pd입니다.", icon: "error" });
      return;
    }
    const deliveryList = addressList
      .filter((el) => el.selected)
      .reduce(
        (prev, curr) => [
          ...prev,
          ...curr.point.reduce(
            (prev, curr) =>
              curr.selected === "selected"
                ? [...prev, ...curr.list.map((el) => el.id)]
                : prev,
            [] as string[]
          ),
        ],
        [] as string[]
      );
    dispatch(
      setAddressToPd({
        id: pd.id,
        point: addressList
          .filter((el) => el.selected)
          .reduce(
            (prev, el) => [
              ...prev,
              ...el.point.filter((pointEl) => pointEl.selected === "selected"),
            ],
            [] as Point[]
          ),
      })
    );
    dispatch(
      setAddressPd({ list: deliveryList, pd: { id: pd.id, name: pd.name } })
    );
    closeModal();
  };
  return (
    <>
      <MapHeader />
      <MapContainer>
        <KakaoMap
          loading={loading}
          pointList={regionSelected}
          clickPoint={clickPoint}
          isShowToolbar={true}
          startRectangle={startRectangle}
          endRectangle={endRectangle}
          selectedpointList={addressList
            .filter((el) => el.selected)
            .reduce(
              (prev, curr) => [
                ...prev,
                ...curr.point.filter(
                  (pointEl) => pointEl.selected === "selected"
                ),
              ],
              [] as Point[]
            )}
        />
        <AssignBtn onClick={handleAssignBtn}>배정하기</AssignBtn>
        <Modal>
          <AssignModal assignPd={submitAssign} pdList={pdList} />
        </Modal>
        <NavContainer>
          <SelectNav
            title="지역선택"
            menus={{
              seletedCount: regionCount,
              list: addressList.map((val) => ({
                selected: val.selected,
                label: val.label,
                isShow: true,
              })),
            }}
            changeSeleted={selectRegion}
          />
        </NavContainer>
      </MapContainer>
    </>
  );
};

export default AssignMap;

const AssignBtn = styled.button`
  position: absolute;
  padding: 12px 18px;
  border-radius: 5px;
  border: 0;
  background-color: #f1c40f;
  font-size: 30px;
  color: white;
  bottom: 20px;
  left: 20px;
  z-index: 10;
  cursor: pointer;
`;

const MapContainer = styled.section`
  position: relative;
  height: calc(100vh - 40px);
  padding-right: 200px;
`;

const NavContainer = styled.div`
  position: absolute;
  right: 0;
  top: 0;
`;
