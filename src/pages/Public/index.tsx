import React from "react";

import { Routes, Route } from "react-router-dom";
import SigninPage from "./SignIn";

const Public: React.FC = () => {
  return (
    <Routes>
      <Route path="/">
        <Route index element={<SigninPage />} />
      </Route>
    </Routes>
  );
};

export default Public;
