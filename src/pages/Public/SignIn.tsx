import React, { FormEvent, useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import styled from "styled-components";
import { loginUserDB } from "../../shared/redux/async/user";
import { resetPd } from "../../shared/redux/Modules/pdSlice";
import { resetUser } from "../../shared/redux/Modules/userSlice";

const SigninPage: React.FC = () => {
  const [id, setId] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const navigation = useNavigate();
  const handleSubmit = (e: FormEvent) => {
    e.preventDefault();
    navigation("/");
    dispatch(resetUser());
    dispatch(resetPd());
    dispatch(loginUserDB({ id, password }));
  };
  return (
    <Container>
      <Image>
        <img src="/images/penguin.png" alt="로고" />
      </Image>
      <h1>로그인</h1>
      <form style={{ width: "300px" }} onSubmit={handleSubmit}>
        <Input>
          <input
            type="text"
            value={id}
            onChange={(e) => setId(e.target.value)}
            placeholder="어드민:admin, pd님:weeat"
          />
        </Input>
        <Input>
          <input
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            placeholder="공통:1234"
          />
        </Input>
        <Button type="submit">로그인</Button>
      </form>
    </Container>
  );
};

export default SigninPage;

const Container = styled.section`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
  font-size: 2em;
`;
const Image = styled.div`
  width: 200px;
  margin: 0 auto 20px;
  img {
    width: 100%;
  }
`;
const Input = styled.div`
  width: 100%;
  input {
    border-radius: 3px;
    width: 100%;
    padding: 8px 12px;
    border: 1px solid #dcdcdc;
  }
`;
const Button = styled.button`
  margin-top: 18px;
  width: 100%;
  font-size: 18px;
  padding: 12px 0;
`;
