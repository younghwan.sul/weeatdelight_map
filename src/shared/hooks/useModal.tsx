import { useState } from "react";
import { createPortal } from "react-dom";
import ModalContainer from "../../components/modal";

const ModalEl = document.querySelector("#modal") as Element;

const useModal = () => {
  const [isShow, setIsShow] = useState(false);
  // const childrenRef = useRef<null | ReactElement>(null);
  const openModal = () => {
    setIsShow(true);
  };
  const closeModal = () => {
    setIsShow(false);
  };
  const ModalComponent: React.FC = ({ children }) =>
    createPortal(
      <ModalContainer isShow={isShow} closeModal={closeModal}>
        {children}
      </ModalContainer>,
      ModalEl
    );
  return { openModal, closeModal, ModalComponent };
};

export default useModal;
