import { createAsyncThunk } from "@reduxjs/toolkit";
import auth, { LoginProps } from "../../../constants/userAuth";
const { userLogin } = auth;

export const loginUserDB = createAsyncThunk(
  "user/login",
  async (data: LoginProps.Input, thunkAPI) => {
    try {
      const response = await userLogin.login(data);
      if (response.status !== 200) {
        const err = {
          message: response.message,
        };
        throw err;
      }

      localStorage.setItem("userTokenTmp", response.data || "");
      return {
        token: response.data || "",
      };
    } catch (error) {
      const err = error as { message: string };
      alert(err.message);
      throw err;
    }
  }
);

export const logoutUserDB = createAsyncThunk(
  "user/logout",
  async (data: string, thunkAPI) => {
    try {
      const response = await userLogin.logout(data);
      if (response.status !== 200) {
        const err = {
          message: response.message,
        };
        throw err;
      }
      localStorage.removeItem("userTokenTmp");
      return;
    } catch (error) {
      const err = error as { message: string };
      alert(err.message);
      throw err;
    }
  }
);
