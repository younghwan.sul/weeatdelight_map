import { createAsyncThunk } from "@reduxjs/toolkit";
import pdsApi, { Point, Pd } from "../../../constants/weeatPd";
// import auth, { LoginProps } from "../../../constants/userAuth";
// const { userLogin } = auth;
export const getPdData = createAsyncThunk("pd/get", async () => {
  try {
    const response = await pdsApi.getPd();
    if (response.status !== 200) {
      const err = {
        message: response.message,
      };
      throw err;
    }

    return {
      pdList: (response.data || []) as Pd[],
    };
  } catch (error) {
    const err = error as { message: string };
    alert(err.message);
    throw err;
  }
});

export const setAddressToPd = createAsyncThunk(
  "pd/setAddress",
  async ({ id, point }: { id: string; point: Point[] }) => {
    try {
      const response = await pdsApi.setPoint(id, point);
      if (response.status !== 200) {
        const err = {
          message: response.message,
        };
        throw err;
      }
      return {
        pdList: (response.data || []) as Pd[],
      };
    } catch (error) {
      const err = error as { message: string };
      alert(err.message);
      throw err;
    }
  }
);

export const getMyList = createAsyncThunk(
  "user/getMyList",
  async ({ id }: { id: string }) => {
    try {
      const response = await pdsApi.getMyDelivery(id);
      if (response.status !== 200) {
        const err = {
          message: response.message,
        };
        throw err;
      }
      console.log(response);
      return {
        deliveryList: (response.data || []) as Point[],
      };
    } catch (error) {
      const err = error as { message: string };
      alert(err.message);
      throw err;
    }
  }
);

export const setMyList = createAsyncThunk(
  "user/setMyList",
  async ({ id, indexList }: { id: string; indexList: number[] }) => {
    try {
      const response = await pdsApi.fixMyDelivery(id, indexList);
      if (response.status !== 200) {
        const err = {
          message: response.message,
        };
        throw err;
      }
      return {
        deliveryList: (response.data || []) as Point[],
      };
    } catch (error) {
      const err = error as { message: string };
      alert(err.message);
      throw err;
    }
  }
);
