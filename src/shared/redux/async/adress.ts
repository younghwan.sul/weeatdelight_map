import { createAsyncThunk } from "@reduxjs/toolkit";
import Swal from "sweetalert2";
import addressApi, { Region } from "../../../constants/address";
export const getAddressData = createAsyncThunk("address/get", async () => {
  try {
    const response = await addressApi.getAddress();
    if (response.status !== 200) {
      const err = {
        message: response.message,
      };
      throw err;
    }
    return {
      addressList: (response.data || []) as Region[],
    };
  } catch (error) {
    const err = error as { message: string };
    alert(err.message);
    throw err;
  }
});

export const setAddressPd = createAsyncThunk(
  "address/setPd",
  async ({
    list,
    pd,
  }: {
    list: string[];
    pd: { id: string; name: string };
  }) => {
    try {
      const response = await addressApi.setPd(list, pd);
      if (response.status !== 200) {
        const err = {
          message: response.message,
        };
        throw err;
      }
      Swal.fire({
        icon: "success",
        title: "배송지가 pd님께 배정되었습니다.",
        confirmButtonText: "확인",
      });
      return {
        addressList: (response.data || []) as Region[],
      };
    } catch (error) {
      const err = error as { message: string };
      alert(err.message);
      throw err;
    }
  }
);
