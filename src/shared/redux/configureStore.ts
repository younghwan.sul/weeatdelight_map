import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import addressSlice from "./Modules/addressSlice";
import pdSlice from "./Modules/pdSlice";
import userSlice from "./Modules/userSlice";

const reducer = combineReducers({
  user: userSlice.reducer,
  pd: pdSlice.reducer,
  address: addressSlice.reducer,
});

export const store = configureStore({
  reducer,
  middleware: [...getDefaultMiddleware()],
});

export type RootState = ReturnType<typeof store.getState>;
