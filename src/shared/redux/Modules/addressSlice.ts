import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Point } from "../../../constants/weeatPd";
import { getAddressData, setAddressPd } from "../async/adress";

export interface RegionMenu {
  label: string;
  selected: boolean;
  point: Point[];
}
interface ReduxState {
  isFetching: boolean;
  addressList: RegionMenu[];
}

const initialState: ReduxState = {
  isFetching: true,
  addressList: [],
};

const addressSlice = createSlice({
  name: "address",
  initialState: initialState,
  reducers: {
    selectAddress: (
      state,
      action: PayloadAction<{ value: number | "all" | "removeAll" }>
    ) => {
      const { value } = action.payload;
      if (value === "all") {
        state.addressList = state.addressList.map((el) => ({
          ...el,
          selected: true,
        }));
      }
      if (value === "removeAll") {
        state.addressList = state.addressList.map((el) => ({
          ...el,
          selected: false,
        }));
      }
      if (typeof value === "number") {
        state.addressList = state.addressList.map((el, idx) => ({
          ...el,
          selected: value === idx ? !el.selected : el.selected,
        }));
      }
    },
    selectPoint: (
      state,
      action: PayloadAction<{ regionIdx: number; pointIdx: number }>
    ) => {
      const { regionIdx, pointIdx } = action.payload;
      state.addressList = state.addressList.map((el, idx) =>
        idx !== regionIdx
          ? { ...el }
          : {
              ...el,
              point: el.point.map((point, idx) => {
                let selected = point.selected;
                if (point.selected === "free") selected = "selected";
                if (point.selected === "selected") selected = "free";
                return idx !== pointIdx ? { ...point } : { ...point, selected };
              }),
            }
      );
    },
    selectRectanglePoint: (
      state,
      action: PayloadAction<{
        start: { x: number; y: number };
        end: { x: number; y: number };
      }>
    ) => {
      const { start, end } = action.payload;
      state.addressList = state.addressList.map((el) => {
        return !el.selected
          ? el
          : {
              ...el,
              point: el.point.map((point) => {
                let selected: Point["selected"] = point.selected;

                if (
                  start &&
                  (Number(point.x) - start.x) * (Number(point.x) - end.x) <=
                    0 &&
                  (Number(point.y) - start.y) * (Number(point.y) - end.y) <= 0
                ) {
                  if (point.selected === "selected") selected = "free";
                  if (point.selected === "free") selected = "selected";
                }
                return { ...point, selected };
              }),
            };
      });
    },
    resetAddress: (state) => {
      state.addressList = state.addressList.map((el, idx) => ({
        ...el,
        selected: true,
        point: el.point.map((pointEl) => ({
          ...pointEl,
          selected: pointEl.selected === "selected" ? "free" : pointEl.selected,
        })),
      }));
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getAddressData.pending, (state) => {
      state.isFetching = true;
    });
    builder.addCase(getAddressData.fulfilled, (state, action) => {
      state.addressList = action.payload.addressList.map((region) => ({
        label: region.name,
        selected: true,
        point: region.point.map((point) => ({
          ...point,
          selected: point.pd ? "assigned" : "free",
        })),
      }));
      state.isFetching = false;
    });

    builder.addCase(getAddressData.rejected, (state) => {
      state.addressList = [];
      state.isFetching = false;
    });

    builder.addCase(setAddressPd.pending, (state) => {
      state.isFetching = true;
    });
    builder.addCase(setAddressPd.fulfilled, (state, action) => {
      state.addressList = action.payload.addressList.map((region) => ({
        label: region.name,
        selected: true,
        point: region.point.map((point) => ({
          ...point,
          selected: point.pd ? "assigned" : "free",
        })),
      }));
      state.isFetching = false;
    });
    builder.addCase(setAddressPd.rejected, (state) => {
      state.addressList = [];
      state.isFetching = false;
    });
  },
});

export default addressSlice;
export const {
  selectAddress,
  selectPoint,
  selectRectanglePoint,
  resetAddress,
} = addressSlice.actions;
