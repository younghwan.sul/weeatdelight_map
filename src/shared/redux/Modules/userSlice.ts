import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Point } from "../../../constants/weeatPd";
import { getMyList, setMyList } from "../async/pd";
import { loginUserDB, logoutUserDB } from "../async/user";

interface ReduxState {
  isFetching: boolean;
  user: {
    token: null | string;
  };
  myList: { isFetching: boolean; list: Point[] | null };
}
const initialState: ReduxState = {
  isFetching: true,
  user: {
    token: null,
  },
  myList: { isFetching: true, list: null },
};

const userSlice = createSlice({
  name: "user",
  initialState: initialState,
  reducers: {
    resetUser: (state) => {
      state.myList.isFetching = true;
    },

    setUser: (state, action: PayloadAction<{ token?: string }>) => {
      if (action.payload.token) {
        state.user = { token: action.payload.token };
        state.isFetching = false;
      } else {
        state.isFetching = false;
        state.user = { token: null };
      }
    },
  },
  extraReducers: (builder) => {
    builder.addCase(loginUserDB.pending, (state) => {
      state.isFetching = true;
    });
    builder.addCase(loginUserDB.fulfilled, (state, action) => {
      state.user = action.payload;
      state.isFetching = false;
    });

    builder.addCase(loginUserDB.rejected, (state) => {
      state.user = { token: null };
      state.isFetching = false;
    });
    builder.addCase(logoutUserDB.pending, (state) => {
      state.isFetching = true;
    });
    builder.addCase(logoutUserDB.fulfilled, (state, action) => {
      state.user = { token: null };
      state.isFetching = false;
    });
    builder.addCase(logoutUserDB.rejected, (state) => {
      state.user = { token: null };
      state.isFetching = false;
    });
    builder.addCase(getMyList.pending, (state) => {
      state.myList.isFetching = true;
    });
    builder.addCase(getMyList.fulfilled, (state, action) => {
      state.myList.isFetching = false;
      state.myList.list = action.payload.deliveryList.map((point, idx) => {
        let selected = point.selected;
        return { ...point, selected };
      });
    });
    builder.addCase(getMyList.rejected, (state) => {
      state.myList.isFetching = false;
    });
    builder.addCase(setMyList.pending, (state) => {
      state.myList.isFetching = true;
    });
    builder.addCase(setMyList.fulfilled, (state, action) => {
      state.myList.isFetching = false;
      state.myList.list = action.payload.deliveryList.map((point, idx) => {
        let selected = point.selected;
        return { ...point, selected };
      });
    });
    builder.addCase(setMyList.rejected, (state) => {
      state.myList.isFetching = false;
    });
  },
});

export const { resetUser } = userSlice.actions;
export default userSlice;
