import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Pd, Point } from "../../../constants/weeatPd";
import { getPdData, setAddressToPd } from "../async/pd";

export interface PdMenu extends Omit<Pd, "deliveryList"> {
  selected: boolean;
  deliveryList: Point[];
}
interface ReduxState {
  isFetching: boolean;
  pdList: PdMenu[];
}

const initialState: ReduxState = {
  isFetching: true,
  pdList: [],
};

const pdSlice = createSlice({
  name: "pd",
  initialState: initialState,
  reducers: {
    selectPd: (state, action: PayloadAction<{ id: string }>) => {
      state.pdList = state.pdList.map((val) => ({
        ...val,
        selected: val.id === action.payload.id ? !val.selected : false,
      }));
    },
    resetPd: (state) => {
      state.isFetching = true;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getPdData.pending, (state) => {
      state.isFetching = true;
    });
    builder.addCase(getPdData.fulfilled, (state, action) => {
      state.pdList = action.payload.pdList.map((el) => ({
        ...el,
        selected: false,
        deliveryList: el.deliveryList.map((el) => ({
          ...el,
          selected: "assigned",
        })),
      }));
      state.isFetching = false;
    });

    builder.addCase(getPdData.rejected, (state) => {
      state.pdList = [];
      state.isFetching = false;
    });

    builder.addCase(setAddressToPd.pending, (state) => {
      state.isFetching = true;
    });
    builder.addCase(setAddressToPd.fulfilled, (state, action) => {
      state.pdList = action.payload.pdList.map((el) => ({
        ...el,
        selected: false,
        deliveryList: el.deliveryList.map((el) => ({
          ...el,
          selected: "assigned",
        })),
      }));
      state.isFetching = false;
    });
    builder.addCase(setAddressToPd.rejected, (state) => {
      state.pdList = [];
      state.isFetching = false;
    });
  },
});

export default pdSlice;
export const { selectPd, resetPd } = pdSlice.actions;
