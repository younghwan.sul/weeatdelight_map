import { createGlobalStyle } from "styled-components";

// 위에서 받은 `normalize`로 기본 css가 초기화 합니다.
const GlobalStyle = createGlobalStyle`

  html,
  body {
    overflow: hidden;
  }

  * {
    box-sizing: border-box;
    padding:0;
    margin:0;
  }
`;

export default GlobalStyle;
