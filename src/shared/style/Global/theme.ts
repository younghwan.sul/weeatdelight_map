import { DefaultTheme } from "styled-components";

export const theme:DefaultTheme = {
  basicWidth:"1200px",
  color:{
    main: "#1c1f25",
    sub: "#fff"

  }
}